const fs=require('fs');
const filename=process.argv[2];
 if (!filename) {
 	throw Error('El fichero no existe');
 }

 fs.watch(filename, ()=>console.log(`El fichero ${filename} se ha modificado`));
 console.log(`Comprobando el estado del fichero ${filename}`);